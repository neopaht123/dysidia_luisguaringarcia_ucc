﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatteryUsed : MonoBehaviour
{
    public float energyBattery = 500f;
    public float dischargingBattery = 500f;
    public Image batteryImage;
    public bool flashlightOn = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (flashlightOn == true)
        {
            batteryImage.fillAmount = dischargingBattery / energyBattery;
            dischargingBattery -= Time.deltaTime;
        }

        if (dischargingBattery >= 0)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                flashlightOn = true;

            }
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                flashlightOn = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            dischargingBattery = 100f;
        }
    }
}

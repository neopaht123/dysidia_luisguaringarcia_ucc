﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

public class Life : MonoBehaviour
{
    
    public Image[] darkerBars;
    public GameObject[] lifeobject;
    public float cooldownLife = 100f;
    public float [] lifeTime;
    public int lifesCores = 0;
    public float waitTime = 0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
       

        if (Input.GetKeyDown(KeyCode.Q))
        {
            waitTime = 0f;
            lifesCores = lifesCores + 1;
        }
        if (lifesCores >= 1)
        {
            waitTime += Time.deltaTime;
            Destroy(lifeobject[0]);
            if (waitTime >= 0.5)
            {
                darkerBars[0].fillAmount = lifeTime[0] / cooldownLife;
                lifeTime[0] -= Time.deltaTime * 40;
            }
        }
        if (lifesCores >= 2)
        {
            waitTime += Time.deltaTime;
            Destroy(lifeobject[1]);
            if (waitTime >= 0.5)
            {
                darkerBars[1].fillAmount = lifeTime[1] / cooldownLife;
                lifeTime[1] -= Time.deltaTime * 40;
               
            }
        }
        if (lifesCores >= 3)
        {
            waitTime += Time.deltaTime;
            Destroy(lifeobject[2]);
            if (waitTime >= 0.5)
            {
                darkerBars[2].fillAmount = lifeTime[2] / cooldownLife;
                lifeTime[2] -= Time.deltaTime * 40;
             
            }
        }
    }
}

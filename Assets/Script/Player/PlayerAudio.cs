﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    public bool isInCathedral = false;
    public bool goingThroughCathedral = false;
    public AudioSource ambient;

    public bool isWalking = false;
    float toNextStep = 0f;
    AudioSource myAudio;
    public AudioClip[] footstepGrass;
    public AudioClip[] footstepTile;


    private void Start()
    {
        myAudio = GetComponent<AudioSource>();
        
    }

    void Update()
    {
        //Footstep System

        if (Input.GetKeyDown(KeyCode.W) ||
            Input.GetKeyDown(KeyCode.A) ||
            Input.GetKeyDown(KeyCode.S) ||
            Input.GetKeyDown(KeyCode.D))
        {
            isWalking = true;
        }
        if (!Input.anyKey)
        {
            isWalking = false;
        }

        if (isWalking)
        {
            toNextStep += Time.deltaTime;
            if (toNextStep >= 0.6f)
            {
                if (!isInCathedral)
                {
                    myAudio.clip = footstepGrass[Random.Range(0, footstepGrass.Length)];
                    myAudio.Play();
                    toNextStep = 0;
                }
                if (isInCathedral)
                {
                    myAudio.clip = footstepTile[Random.Range(0, footstepTile.Length)];
                    myAudio.Play();
                    toNextStep = 0;
                }

            }
        }

        if (goingThroughCathedral && !isInCathedral)
        {
            ambient.GetComponent<AudioSource>().volume += 0.01f;
        }
        else if (goingThroughCathedral && isInCathedral)
        {
            ambient.GetComponent<AudioSource>().volume -= 0.01f;
        }
        Debug.Log(ambient.GetComponent<AudioSource>().volume);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isInCathedral == false)
        {
            if (other.gameObject.name == "CathedralTrigger")
            {
                isInCathedral = true;
                goingThroughCathedral = true;
            }
        }
        else if (isInCathedral == true)
        {
            if (other.gameObject.name == "CathedralTrigger")
            {
                isInCathedral = false;
                goingThroughCathedral = true;
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Linterna : MonoBehaviour
{
    Light linterna;
    GameObject audio;
    bool encender = true;
    // Start is called before the first frame update
    void Start()
    {
        linterna = GetComponent<Light>();
        audio = GameObject.FindGameObjectWithTag("flashlight");
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetKeyDown(KeyCode.F)){
            encender = !encender;
            audio.GetComponent<AudioSource>().Play();
        }
        if(encender){
            linterna.enabled = true;
            
        }else{
            linterna.enabled = false;
        }
    }
}

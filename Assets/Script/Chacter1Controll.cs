﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Chacter1Controll : MonoBehaviour
{
    public float velocidadMovimiento = 5.0f;
    public float velocidadRotacion = 200.0f;
    private Animator anim;
    public float x, y;
    private int vida = 100;
    private Rigidbody rb; 
    public Slider slidervida;
    
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        ActualizarVida();
        if(vida <= 0){
            Muerte();
        }

        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
        transform.Translate(0, 0, y * Time.deltaTime * velocidadMovimiento);

        anim.SetFloat("Velx", x);
        anim.SetFloat("Vely", y);
    }

    
    public void QuitarVida(int perdidaVida){
        vida = vida - perdidaVida;
        Debug.Log("Nivel de vida >>> " + vida);
    }

    public void Muerte(){
        SceneManager.LoadScene("Menu Screen");
    }

    public void ActualizarVida(){
        slidervida.value = (float)vida;
    }
}

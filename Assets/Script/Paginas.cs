﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Paginas : MonoBehaviour
{
    public Text periodicos;
	GameObject audio;
	GameObject audioPiano;
    int contadorPeriodicos = 0;
    GameObject player;
    GameObject paper1;
    GameObject paper2;
    GameObject paper3;
    GameObject paper4;
    GameObject paper5;
    GameObject paper6;
    GameObject paper7;
    GameObject paper8;
    GameObject piano;
    bool atrapada1 = false;
    bool atrapada2 = false;
    bool atrapada3 = false;
    bool atrapada4 = false;
    bool atrapada5 = false;
    bool atrapada6 = false;
    bool atrapada7 = false;
    bool atrapada8 = false;
    bool pianoInteract = false;
    // Start is called before the first frame update
    void Start()
    {
	piano = GameObject.Find("Piano");
    paper1 = GameObject.Find("pagina1");
	paper2 = GameObject.Find("pagina2");
	paper3 = GameObject.Find("pagina3");
	paper4 = GameObject.Find("pagina4");
	paper5 = GameObject.Find("pagina5");
	paper6 = GameObject.Find("pagina6");
	paper7 = GameObject.Find("pagina7");
	paper8 = GameObject.Find("pagina8");
	player = GameObject.Find("FirstPersonCharacter");
	audio = GameObject.FindGameObjectWithTag("page");
	piano = GameObject.Find("Piano");
	audioPiano = GameObject.FindGameObjectWithTag("piano");
    }

    // Update is called once per frame
    void Update()
    {
        //Pagina 1
	var distance = Vector3.Distance(paper1.transform.position, player.transform.position);
	if(distance < 2 && Input.GetMouseButtonUp(0) && atrapada1 == false){
		contadorPeriodicos+=1;
		audio.GetComponent<AudioSource>().Play();
		GetComponent<AudioSource>().Play();
        periodicos.text = "Periodicos recolectados: " + contadorPeriodicos;
		paper1.GetComponent<Renderer>().enabled = false;
		atrapada1 = true;
	}
	
	//Pagina 2
	var distance2 = Vector3.Distance(paper2.transform.position, player.transform.position);
	if(distance2 < 2 && Input.GetMouseButtonUp(0) && atrapada2 == false){
		contadorPeriodicos+=1;
		audio.GetComponent<AudioSource>().Play();
        periodicos.text = "Periodicos recolectados: " + contadorPeriodicos;
		GetComponent<AudioSource>().Play();
		paper2.GetComponent<Renderer>().enabled = false;
		atrapada2 = true;
	}
	
	//Pagina 3
	var distance3 = Vector3.Distance(paper3.transform.position, player.transform.position);
	if(distance3 < 2 && Input.GetMouseButtonUp(0) && atrapada3 == false){
		contadorPeriodicos+=1;
		audio.GetComponent<AudioSource>().Play();
        periodicos.text = "Periodicos recolectados: " + contadorPeriodicos;
		GetComponent<AudioSource>().Play();
		paper3.GetComponent<Renderer>().enabled = false;
		atrapada3 = true;
	}
	
		//Pagina 4
	var distance4 = Vector3.Distance(paper4.transform.position, player.transform.position);
	if(distance4 < 2 && Input.GetMouseButtonUp(0) && atrapada4 == false){
		contadorPeriodicos+=1;
		audio.GetComponent<AudioSource>().Play();
        periodicos.text = "Periodicos recolectados: " + contadorPeriodicos;
		GetComponent<AudioSource>().Play();
		paper4.GetComponent<Renderer>().enabled = false;
		atrapada4 = true;
	}
	
		//Pagina 5
	var distance5 = Vector3.Distance(paper5.transform.position, player.transform.position);
	if(distance5 < 2 && Input.GetMouseButtonUp(0) && atrapada5 == false){
		contadorPeriodicos+=1;
		audio.GetComponent<AudioSource>().Play();
        periodicos.text = "Periodicos recolectados: " + contadorPeriodicos;
		GetComponent<AudioSource>().Play();
		paper5.GetComponent<Renderer>().enabled = false;
		atrapada5 = true;
	}
	
		//Pagina 6
	var distance6 = Vector3.Distance(paper6.transform.position, player.transform.position);
	if(distance6 < 2 && Input.GetMouseButtonUp(0) && atrapada6 == false){
		contadorPeriodicos+=1;
		audio.GetComponent<AudioSource>().Play();
        periodicos.text = "Periodicos recolectados: " + contadorPeriodicos;
		GetComponent<AudioSource>().Play();
		paper6.GetComponent<Renderer>().enabled = false;
		atrapada6 = true;
	}
	
		//Pagina 7
	var distance7 = Vector3.Distance(paper7.transform.position, player.transform.position);
	if(distance7 < 2 && Input.GetMouseButtonUp(0) && atrapada7 == false){
		contadorPeriodicos+=1;
		audio.GetComponent<AudioSource>().Play();
        periodicos.text = "Periodicos recolectados: " + contadorPeriodicos;
		GetComponent<AudioSource>().Play();
		paper7.GetComponent<Renderer>().enabled = false;
		atrapada7 = true;
	}
	
		//Pagina 8
	var distance8 = Vector3.Distance(paper8.transform.position, player.transform.position);
	if(distance8 < 2 && Input.GetMouseButtonUp(0) && atrapada8 == false){
		contadorPeriodicos+=1;
		audio.GetComponent<AudioSource>().Play();
        periodicos.text = "Periodicos recolectados: " + contadorPeriodicos;
		GetComponent<AudioSource>().Play();
		paper8.GetComponent<Renderer>().enabled = false;
		atrapada8 = true;
	}

	var distance9 = Vector3.Distance(piano.transform.position, player.transform.position);
	if(distance9 < 2 && Input.GetMouseButtonUp(0) && pianoInteract == false){
		audioPiano.GetComponent<AudioSource>().Play();
		GetComponent<AudioSource>().Play();
		pianoInteract = true;
	}
	
	//Vittoria
	if (contadorPeriodicos == 8){
		SceneManager.LoadScene("");
	}
    }
}

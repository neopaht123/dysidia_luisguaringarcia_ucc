﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbrirPuertaHouse : MonoBehaviour
{
public float speed;
public float angle;
private Vector3 direction;
public bool abrir;
public bool cerrado;
    // Start is called before the first frame update
    void Start()
    {
    angle = transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
    if(Mathf.Round(transform.eulerAngles.y) != angle){
        transform.Rotate(direction * speed);
    }

    if(Input.GetKeyDown("e") && abrir == true && cerrado == false){
        angle = 90;
        direction = Vector3.up;
        cerrado = true;
     }else if(Input.GetKeyDown("e") && abrir == true && cerrado == true){
        angle = 0;
        direction = Vector3.down;
        cerrado = false;
        }
    }

    public void OnGUI(){
        if(abrir){
         GUI.Label(new Rect(Screen.width/2 - 75, Screen.height - 100, 150, 30), "Presiona 'E' para abrir la puerta");
        }
    }

    public void OnTriggerEnter (Collider other){
        if (other.gameObject.tag == "Player") {
            abrir = true;
        }
    }

    //Deactivate the Main function when player is go away from door
    public void OnTriggerExit (Collider other){
        if (other.gameObject.tag == "Player") {
            abrir = false;
        }
    }
}

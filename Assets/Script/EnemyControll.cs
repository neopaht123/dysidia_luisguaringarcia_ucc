﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControll : MonoBehaviour
{
    public Chacter1Controll character;
    public Transform objetivo;
    private Animator anim;
    float pantalla = 0.8f;
    public float velocidadRotacion =200.0f;
    Rigidbody rb;
    public bool Visible = false;
    public float velocidad = 10;
    public float punto;
    bool avanzando = true;
    bool quieto = false;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        Esvisible();
        transform.LookAt(objetivo.position);
        if (!Visible)
        {
           // rb.MovePosition(transform.position + transform.forward * Time.deltaTime*velocidad);
            transform.position = transform.position + transform.forward * velocidad * Time.deltaTime;
        }
	}

    void Esvisible()
    {
        Vector3 delante = objetivo.forward;
        Vector3 otro = (this.transform.position - objetivo.position).normalized;
        punto = Vector3.Dot(delante, otro);
        if (punto > pantalla)
        {
            Visible = true;
            anim.SetBool("Walk", quieto);
        }
        else
        {
            Visible = false;
            anim.SetBool("Walk", avanzando);
        }
    }

    void OnTriggerEnter(Collider objeto){
        if(objeto.tag == "Player"){
            character.QuitarVida(10);
        }
    }
}


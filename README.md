# Dysidia
Videojuego desarrollado por Luis Angel Guarin Garcia.
Universidad cooperativa de Colombia
Sede Popayán

Contacto: neopaht123@gmail.com - luis.guaring@campusucc.edu.co

# Descripción
El objetivo del videojuego es resolver un misterio que lleva años atemorizando a la población de “Milenario del valle”, un pueblo ficticio ubicado en el departamento de Cundinamarca – Colombia, que está alejado de la civilización y donde viven pocas personas, la personaje principal deberá hallar las pistas que la conduzcan al núcleo de este misterio, que prontamente se convertirá en una pesadilla.

# Información
- Nombre: Dysidia "La invasión"
- Plataforma: Windows PC
- Desarrollado en: Unity 3D
- Programado en: C#
